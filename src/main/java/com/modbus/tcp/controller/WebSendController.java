package com.modbus.tcp.controller;//package com.modbus.tcp.controller;

import com.modbus.tcp.client.ChatRoomServerEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/web")
public class WebSendController {

    @Autowired
    private ChatRoomServerEndpoint webSocketClient;

    @RequestMapping(value = "/sendToOne", produces = {"application/json; charset=utf-8"}, method = RequestMethod.POST)
    public void sendToOne(String message) {
        webSocketClient.sendMessage(webSocketClient.livingSessions.values().parallelStream().collect(Collectors.toList()).get(0), message);
    }
}
